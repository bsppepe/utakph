import { NewUserValidator } from "@/backend/models/user";
import UsersServices from "@/backend/services/userServices";
import payloadValidator from "@/utils/payloadValidator";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<any> => {
  try {
    const value = await payloadValidator(NewUserValidator, req.body);

    const response = await UsersServices.createUser(value);

    return res.status(200).json({
      success: true,
      code: 200,
      data: response,
    });
  } catch (error: any) {
    return res.status(400).json({
      success: false,
      code: 400,
      error,
    });
  }
};

export default handler;
