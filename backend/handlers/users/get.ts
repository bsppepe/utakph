import UsersServices from "@/backend/services/userServices";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<any> => {
  try {
    const { id } = req.query;

    let response;

    if (!id) {
      response = await UsersServices.getUsers();
    } else {
      response = await UsersServices.getUser(<string>id);
    }

    return res.status(200).json({
      success: true,
      code: 200,
      data: response,
    });
  } catch (error: any) {
    return res.status(400).json({
      success: false,
      code: 400,
      error,
    });
  }
};

export default handler;
