import { UpdateProductValidator } from "@/backend/models/products";
import ProductsServices from "@/backend/services/productServices";
import payloadValidator from "@/utils/payloadValidator";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<any> => {
  try {
    const value = await payloadValidator(UpdateProductValidator, req.body);

    const response = await ProductsServices.updateProduct(value);

    return res.status(200).json({
      success: true,
      code: 200,
      data: response,
    });
  } catch (error: any) {
    return res.status(400).json({
      success: false,
      code: 400,
      error,
    });
  }
};

export default handler;
