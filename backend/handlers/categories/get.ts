import CategoriesServices from "@/backend/services/categoryServices";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<any> => {
  try {
    const { id, userId } = req.query;

    let response;

    if (userId) {
      response = await CategoriesServices.getCategoryList(<string>userId);
    }
    if (id) {
      response = await CategoriesServices.getCategoryById(<string>id);
    }

    return res.status(200).json({
      success: true,
      code: 200,
      data: response,
    });
  } catch (error: any) {
    return res.status(400).json({
      success: false,
      code: 400,
      error,
    });
  }
};

export default handler;
