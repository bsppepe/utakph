import firebaseDB from "@/lib/firebaseDB";
import {
  IFilterProducts,
  IProduct,
  IUpdateProduct,
} from "./../models/products";
import { DATABASE_ENTRIES, FUNCTIONS } from "@/utils/constants";
import { v4 as uuidv4 } from "uuid";
import { isBoolean, isEmpty } from "lodash";
import { checkBoolean } from "@/utils/helper";
import CategoriesServices from "./categoryServices";

interface IProductServices {
  addProduct(args: IProduct): Promise<any>;
  updateProduct(args: any): Promise<any>;
  getProduct(args: string): Promise<any>;
  getProducts(args: IFilterProducts): Promise<any>;
  removeProduct(args: any): Promise<any>;
}

const ProductsServices: IProductServices = {
  addProduct: async (args: IProduct) => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.PRODUCTS,
        func: FUNCTIONS.CREATE,
        id: uuidv4(),
        params: args,
      });

      console.log({
        id: "add-product",
        args,
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "add-product-error",
        args,
        error,
      });
      throw error?.message;
    }
  },
  updateProduct: async (args: IUpdateProduct | any) => {
    try {
      const id = args.id;
      const params = Object.keys(args)
        .filter((objKey) => objKey !== "id")
        .reduce((newObj: any, key: any) => {
          newObj[key] = args[key];
          return newObj;
        }, {});

      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.PRODUCTS,
        func: FUNCTIONS.UPDATE,
        id,
        params,
      });

      console.log({
        id: "update-product",
        args,
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "update-product-error",
        args,
        error,
      });
      throw error?.message;
    }
  },
  getProduct: async (id: string) => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.PRODUCTS,
        func: FUNCTIONS.READ,
        id,
      });

      if (isEmpty(response)) throw new Error("Product does not exist");

      console.log({
        id: "get-product",
        args: { id },
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "get-product-error",
        args: { id },
        error,
      });
      throw error?.message;
    }
  },
  getProducts: async (args: IFilterProducts) => {
    const { userId } = args;
    try {
      const rawResponse = await firebaseDB({
        entry: DATABASE_ENTRIES.PRODUCTS,
        func: FUNCTIONS.LIST,
      });

      const filteredProducts = await Promise.all(
        rawResponse.map(async (product: IProduct) => {
          const productUserId = product.userId;

          if (productUserId === userId) {
            const categoryData = await CategoriesServices.getCategoryById(
              product.category
            );
            return {
              ...product,
              category: categoryData.name,
            };
          }

          return null;
        })
      );

      const response = filteredProducts.filter((product) => product !== null);

      console.log({
        id: "get-product",
        args,
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "get-products-error",
        args,
        error,
      });
      throw error?.message;
    }
  },
  removeProduct: async (id: string) => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.PRODUCTS,
        func: FUNCTIONS.DELETE,
        id,
      });

      console.log({
        id: "remove-product",
        args: { id },
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "remove-product-error",
        args: { id },
        error,
      });
      throw error?.message;
    }
  },
};

export default ProductsServices;
