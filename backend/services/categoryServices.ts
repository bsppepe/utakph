import firebaseDB from "@/lib/firebaseDB";
import { ICategory } from "../models/categories";
import { DATABASE_ENTRIES, FUNCTIONS } from "@/utils/constants";
import { v4 as uuidv4 } from "uuid";
import { isEmpty } from "lodash";

interface ICategoryServices {
  addCategory(args: ICategory): Promise<any>;
  getCategoryById(id: string): Promise<any>;
  getCategoryList(userId: string): Promise<any>;
}

const CategoriesServices: ICategoryServices = {
  addCategory: async (args: ICategory) => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.CATEGORIES,
        func: FUNCTIONS.CREATE,
        id: uuidv4(),
        params: args,
      });

      console.log({
        id: "add-category",
        args,
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "add-category-error",
        args,
        error,
      });
      throw error?.message;
    }
  },
  getCategoryById: async (id: string) => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.CATEGORIES,
        func: FUNCTIONS.READ,
        id,
      });

      if (isEmpty(response)) throw new Error("Category does not exist");

      console.log({
        id: "get-category-by-id",
        args: { id },
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "get-category-by-id-error",
        args: { id },
        error,
      });
      throw error?.message;
    }
  },
  getCategoryList: async (userId: string) => {
    console.log({ userId });
    try {
      const rawResponse = await firebaseDB({
        entry: DATABASE_ENTRIES.CATEGORIES,
        func: FUNCTIONS.LIST,
      });

      const response = rawResponse.filter((item: any) => {
        if (item.userId !== userId) return;
        return item;
      });

      console.log({
        id: "get-categories",
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "get-categories-error",
        error,
      });
      throw error?.message;
    }
  },
};

export default CategoriesServices;
