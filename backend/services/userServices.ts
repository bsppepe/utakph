import firebaseDB from "@/lib/firebaseDB";
import { DATABASE_ENTRIES, FUNCTIONS } from "@/utils/constants";
import { v4 as uuidv4 } from "uuid";
import { IUser } from "../models/user";
import { isEmpty } from "lodash";

interface IUserServices {
  createUser(args: IUser): Promise<any>;
  getUser(id: string): Promise<any>;
  getUsers(): Promise<any>;
}

const UsersServices: IUserServices = {
  createUser: async (args: IUser) => {
    try {
      const exist = await firebaseDB({
        entry: DATABASE_ENTRIES.USERS,
        func: FUNCTIONS.EXIST,
        exist: {
          key: "emailAddress",
          value: args.emailAddress,
        },
      });

      if (exist) throw new Error("Email address already registered");

      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.USERS,
        func: FUNCTIONS.CREATE,
        id: uuidv4(),
        params: args,
      });

      console.log({
        id: "create-user",
        args,
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "create-user-error",
        args,
        error,
      });
      throw error?.message;
    }
  },
  getUser: async (id: string) => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.USERS,
        func: FUNCTIONS.READ,
        id,
      });

      if (isEmpty(response)) throw new Error("User does not exist");

      console.log({
        id: "get-user",
        args: { id },
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "get-user-error",
        args: { id },
        error,
      });
      throw error?.message;
    }
  },
  getUsers: async () => {
    try {
      const response = await firebaseDB({
        entry: DATABASE_ENTRIES.USERS,
        func: FUNCTIONS.LIST,
      });

      console.log({
        id: "get-users",
        response,
      });

      return response;
    } catch (error: any) {
      console.error({
        id: "get-users-error",
        error,
      });
      throw error?.message;
    }
  },
};

export default UsersServices;
