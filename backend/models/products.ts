import { SIZES } from "@/utils/constants";
import Joi from "joi";

export interface IProduct {
  userId: string;
  name: string;
  amountInStock: number;
  cost: number;
  category: string;
  size: SIZES[];
  isAvailable: boolean;
}

export interface IUpdateProduct extends IProduct {
  id: string;
}

export interface IFilterProducts {
  userId: string;
  category?: string;
  isAvailable?: string;
}

export const NewProductValidator = Joi.object({
  userId: Joi.string().required().trim(),
  name: Joi.string().required().trim(),
  amountInStock: Joi.number().required(),
  cost: Joi.number().required(),
  category: Joi.string().required().trim(),
  sizes: Joi.array()
    .items(
      Joi.string()
        .required()
        .allow(
          SIZES.EXTRA_LARGE,
          SIZES.LARGE,
          SIZES.MEDIUM,
          SIZES.SHARING,
          SIZES.SMALL,
          SIZES.SOLO
        )
    )
    .required(),
  isAvailable: Joi.boolean().optional().allow(null),
});

export const UpdateProductValidator = Joi.object({
  id: Joi.string().required().trim(),
  userId: Joi.string().required().trim(),
  name: Joi.string().required().trim(),
  amountInStock: Joi.number().required(),
  cost: Joi.number().required(),
  category: Joi.string().required().trim(),
  sizes: Joi.array()
    .items(
      Joi.string()
        .required()
        .allow(
          SIZES.EXTRA_LARGE,
          SIZES.LARGE,
          SIZES.MEDIUM,
          SIZES.SHARING,
          SIZES.SMALL,
          SIZES.SOLO
        )
    )
    .required(),
  isAvailable: Joi.boolean().optional().allow(null),
});
