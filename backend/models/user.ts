import Joi from "joi";

export interface IUser {
  name: string;
  emailAddress: string;
  products?: string[];
}

export const NewUserValidator = Joi.object({
  name: Joi.string().required().trim(),
  emailAddress: Joi.string().email().required().trim(),
});
