import Joi from "joi";

export interface ICategory {
  userId: string;
  name: string;
  description: string;
}

export const NewCategoryValidator = Joi.object({
  userId: Joi.string().required().trim(),
  name: Joi.string().required().trim(),
  description: Joi.string().required().trim(),
});
