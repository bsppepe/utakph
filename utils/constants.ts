const constants = {
  BASE_URL: <string>process.env.BASE_URL,
  FIREBASE_API_KEY: <string>process.env.FIREBASE_API_KEY,
  FIREBASE_AUTH_DOMAIN: <string>process.env.FIREBASE_AUTH_DOMAIN,
  FIREBASE_DATABASE_URL: <string>process.env.FIREBASE_DATABASE_URL,
  FIREBASE_PROJECT_ID: <string>process.env.FIREBASE_PROJECT_ID,
  FIREBASE_STORAGE_BUCKET: <string>process.env.FIREBASE_STORAGE_BUCKET,
  FIREBASE_MESSAGING_SENDER_ID: <string>(
    process.env.FIREBASE_MESSAGING_SENDER_ID
  ),
  FIREBASE_APP_ID: <string>process.env.FIREBASE_APP_ID,
  FIREBASE_MEASUREMENT_ID: <string>process.env.FIREBASE_MEASUREMENT_ID,
};

export enum DATABASE_ENTRIES {
  USERS = "Users",
  PRODUCTS = "Products",
  CATEGORIES = "Categories",
}

export enum FUNCTIONS {
  CREATE = "create",
  READ = "read",
  UPDATE = "update",
  DELETE = "delete",
  LIST = "get-list",
  EXIST = "check-exist",
}

export enum SIZES {
  SMALL = "Small",
  MEDIUM = "Medium",
  LARGE = "Large",
  EXTRA_LARGE = "Extra Large",
  SOLO = "Solo",
  SHARING = "Sharing",
}

export const SIZE_SELECTION = [
  SIZES.SMALL,
  SIZES.MEDIUM,
  SIZES.LARGE,
  SIZES.EXTRA_LARGE,
  SIZES.SOLO,
  SIZES.SHARING,
];

export default constants;
