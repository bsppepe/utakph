export const checkBoolean = (stringValue: string) => {
  let booleanValue;

  if (stringValue.toLowerCase() === "true") {
    booleanValue = true;
  } else if (stringValue.toLowerCase() === "false") {
    booleanValue = false;
  }

  return booleanValue;
};
