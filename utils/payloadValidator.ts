const payloadValidator = async (schema: any, payload: any) => {
  const { error, value } = await schema.validate(payload, {
    abortEarly: false,
    allowUnknown: true,
  });
  if (error) {
    console.error({
      id: "payload-validation-error",
      payload,
      response: value,
      error,
    });
    throw new Error(error);
  }
  console.log({
    id: "payload-validation",
    payload,
    response: value,
  });
  return value;
};

export default payloadValidator;
