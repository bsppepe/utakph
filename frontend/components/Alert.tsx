import Alert, { AlertColor } from "@mui/material/Alert";

interface IAlertParams {
  type: AlertColor;
  content: string;
}

const CustomAlert = (args: IAlertParams) => {
  const { content, type } = args;

  return (
    <Alert severity={type} style={{ margin: "1rem" }}>
      {content}
    </Alert>
  );
};

export default CustomAlert;
