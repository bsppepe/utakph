import { Button, Grid } from "@mui/material";
import { useCallback, useContext, useEffect, useState } from "react";
import { UserContext } from "../providers/UserProvider";
import ProductCard from "./ProductCard";
import AddProductModal from "./AddProductModal";
import AddCategoryModal from "./AddCategoryModal";

const ProductsContainer = () => {
  // @ts-ignore
  const { userData } = useContext(UserContext);

  // New Products
  const [openModal, setOpenModal] = useState<boolean>(false);
  const openNewProductModal = () => {
    setOpenModal(true);
  };
  const closeNewProductModal = () => {
    setOpenModal(false);
  };

  // New Categories
  const [openCategoryModal, setOpenCategoryModal] = useState<boolean>(false);
  const openNewCategoryModal = () => {
    setOpenCategoryModal(true);
  };
  const closeNewCategoryModal = () => {
    setOpenCategoryModal(false);
  };

  const [categories, setCategories] = useState<any[]>([]);

  const fetchCategories = useCallback(async () => {
    try {
      const rawResponse = await fetch(
        `/api/categories?userId=${userData.userId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (!rawResponse.ok) throw new Error("Unable to fetch categories");

      const response = await rawResponse.json();

      setCategories(response.data);
    } catch (error: any) {
      setCategories([]);
    }
  }, [userData.userId]);

  const refetchCategories = () => fetchCategories();

  useEffect(() => {
    fetchCategories();
  }, [fetchCategories]);

  const [products, setProducts] = useState([]);
  const fetchUserProducts = useCallback(async () => {
    try {
      const url = `/api/products?userId=${userData.userId}`;
      const rawResponse = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const response = await rawResponse.json();
      console.log({ id: "fetch-products", response });
      setProducts(response.data);
    } catch (error: any) {
      setProducts([]);
      throw new Error(error?.message);
    }
  }, [userData]);

  const refetchProducts = () => fetchUserProducts();

  useEffect(() => {
    fetchUserProducts();
  }, [fetchUserProducts]);

  return (
    <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, lg: 12 }}>
      <Grid
        item
        xs={12}
        style={{
          margin: "1rem",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Button variant="outlined" onClick={openNewCategoryModal}>
          Create Category
        </Button>
        <AddCategoryModal
          closeNewCategoryModal={closeNewCategoryModal}
          open={openCategoryModal}
          userId={userData.userId}
          refetchCategories={refetchCategories}
        />

        <Button variant="outlined" onClick={openNewProductModal}>
          Add Product
        </Button>
        <AddProductModal
          closeNewProductModal={closeNewProductModal}
          open={openModal}
          userId={userData.userId}
          categories={categories}
          refetchProducts={refetchProducts}
        />
      </Grid>

      {products &&
        products.length > 0 &&
        products.map(
          ({
            userId,
            amountInStock,
            category,
            cost,
            isAvailable,
            name,
            sizes,
            id,
          }) => {
            return (
              <Grid item xs={12} md={6} lg={3} key={id}>
                <ProductCard
                  key={id}
                  userId={userId}
                  amountInStock={amountInStock}
                  category={category}
                  cost={cost}
                  isAvailable={isAvailable}
                  name={name}
                  size={sizes}
                  productId={id}
                  categories={categories}
                  refetchProducts={refetchProducts}
                />
              </Grid>
            );
          }
        )}
    </Grid>
  );
};

export default ProductsContainer;
