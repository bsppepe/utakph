import {
  Box,
  Button,
  Grid,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import { modalStyle } from "../pages/UserSelectionPage/styles";
import React, { useEffect, useState } from "react";
import CustomAlert from "./Alert";
import { isBoolean } from "lodash";

interface IAddUserModal {
  openCreateUserModal: any;
  handleCloseAddUser: any;
  name: any;
  email: any;
  setName: any;
  setEmail: any;
  fetchUsers: () => {};
}

interface IAddUser {
  name: any;
  email: any;
  fetchUsers: () => void;
  handleCloseAddUser: () => void;
}

const ConfirmNewUser = ({
  name,
  email,
  fetchUsers,
  handleCloseAddUser,
}: IAddUser) => {
  const [modalWidth, setModalWidth] = useState("50%");

  useEffect(() => {
    if (typeof window !== "undefined") {
      const isSmallScreen = window.matchMedia("(max-width: 600px)").matches;
      setModalWidth(isSmallScreen ? "90%" : "50%");
    }
  }, []);

  const [open, setOpen] = useState(false);
  const [requestSuccess, setRequestSuccess] = useState<boolean | null>(null);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async () => {
    const response = await fetch(`/api/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        emailAddress: email,
      }),
    });

    if (response.ok) {
      setOpen(false);
      fetchUsers();
      setRequestSuccess(true);
    } else {
      setOpen(false);
      setRequestSuccess(false);
    }

    setTimeout(() => {
      handleCloseAddUser();
    }, 3000);
  };

  return (
    <Grid container xs={12}>
      <Grid
        item
        xs={12}
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {!name || !email ? (
          <Button variant="contained" disabled>
            Submit
          </Button>
        ) : (
          <Button onClick={handleOpen} variant="contained">
            Submit
          </Button>
        )}
      </Grid>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box
          sx={{
            ...modalStyle,
            width: modalWidth,
            justifyContent: "space-between",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Grid item xs={12}>
              <Typography variant="h6">Confirm details</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body1">{`Name: ${
                name ? name : ""
              }`}</Typography>
              <Typography variant="body1">
                {`Email: ${email ? email : ""}`}
              </Typography>
            </Grid>
            <Grid
              item
              xs={12}
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Button onClick={handleSubmit} variant="outlined" color="success">
                Submit
              </Button>
              <Button onClick={handleClose} color="error" variant="outlined">
                Cancel
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Grid item xs={12}>
        {isBoolean(requestSuccess) && requestSuccess && (
          <CustomAlert type="success" content="User successfully saved" />
        )}
        {isBoolean(requestSuccess) && !requestSuccess && (
          <CustomAlert
            type="error"
            content="Unable to save user. Please try again"
          />
        )}
      </Grid>
    </Grid>
  );
};

const AddUserModal = ({
  fetchUsers,
  email,
  setEmail,
  name,
  setName,
  openCreateUserModal,
  handleCloseAddUser,
}: IAddUserModal) => {
  const [modalWidth, setModalWidth] = useState("50%");

  useEffect(() => {
    if (typeof window !== "undefined") {
      const isSmallScreen = window.matchMedia("(max-width: 600px)").matches;
      setModalWidth(isSmallScreen ? "90%" : "50%");
    }
  }, []);

  return (
    <Modal open={openCreateUserModal} onClose={handleCloseAddUser}>
      <Box sx={{ ...modalStyle, width: modalWidth }}>
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid item xs={12}>
            <Typography variant="h5">Add User</Typography>
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              label="Name"
              style={{ width: "100%" }}
              onChange={(e: any) => {
                setName(e.target.value);
              }}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              label="Email Address"
              style={{ width: "100%" }}
              onChange={(e: any) => {
                setEmail(e.target.value);
              }}
            />
          </Grid>
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <ConfirmNewUser
              name={name}
              email={email}
              fetchUsers={fetchUsers}
              handleCloseAddUser={handleCloseAddUser}
            />
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
};

export default AddUserModal;
