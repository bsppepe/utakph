import {
  Autocomplete,
  Box,
  Button,
  Grid,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import { modalStyle } from "../pages/UserSelectionPage/styles";
import React, { useContext, useEffect, useState } from "react";
import { UserContext } from "../providers/UserProvider";

interface ISelectUserModal {
  openSelectUserModal: any;
  handleCloseSelectUser: any;
  users: any;
}

const SelectUserModal = ({
  users,
  openSelectUserModal,
  handleCloseSelectUser,
}: ISelectUserModal) => {
  const [modalWidth, setModalWidth] = useState("50%");

  useEffect(() => {
    if (typeof window !== "undefined") {
      const isSmallScreen = window.matchMedia("(max-width: 600px)").matches;
      setModalWidth(isSmallScreen ? "90%" : "50%");
    }
  }, []);

  //@ts-ignore
  const { updateUser } = useContext(UserContext);

  const [selectedUser, setSelectedUser] = useState(null);

  const handleSelectedUser = () => {
    updateUser(selectedUser);
  };

  return (
    <Modal open={openSelectUserModal} onClose={handleCloseSelectUser}>
      <Box sx={{ ...modalStyle, width: modalWidth }}>
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid item xs={12}>
            <Typography variant="h5">Select User</Typography>
          </Grid>
          <Grid item xs={12}>
            {users && (
              <Autocomplete
                disablePortal
                options={users}
                getOptionLabel={(user: any) =>
                  `${user?.name} (${user?.emailAddress})`
                }
                renderInput={(params) => (
                  <TextField {...params} label="Users" />
                )}
                onChange={(event: any, newValue: any) => {
                  setSelectedUser({
                    //@ts-ignore
                    type: "ADD_USER",
                    userId: newValue.id,
                    name: newValue.name,
                    emailAddress: newValue.emailAddress,
                  });
                }}
              />
            )}
            {!users && (
              <Autocomplete
                options={[]}
                disabled={true}
                renderInput={(params) => (
                  <TextField {...params} label="Users" />
                )}
              />
            )}
          </Grid>
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Button
              style={{ margin: "1rem" }}
              onClick={handleSelectedUser}
              variant="contained"
            >
              Confirm
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
};

export default SelectUserModal;
