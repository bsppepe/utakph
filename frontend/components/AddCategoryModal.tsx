import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  FormControl,
  Grid,
  Input,
  InputLabel,
  Modal,
  Typography,
} from "@mui/material";
import { modalStyle } from "../pages/UserSelectionPage/styles";
import CustomAlert from "./Alert";
import { isBoolean } from "lodash";

interface INewCategoryModal {
  open: boolean;
  closeNewCategoryModal: () => void;
  refetchCategories: () => void;
  userId: string;
}

interface INewCategoryParams {
  name: string;
  description: string;
  userId: string;
}

const AddCategoryModal = (args: INewCategoryModal) => {
  const { userId, open, closeNewCategoryModal, refetchCategories } = args;

  const [modalWidth, setModalWidth] = useState("50%");

  useEffect(() => {
    if (typeof window !== "undefined") {
      const isSmallScreen = window.matchMedia("(max-width: 600px)").matches;
      setModalWidth(isSmallScreen ? "90%" : "50%");
    }
  }, []);

  const [formData, setFormData] = useState<INewCategoryParams>({
    name: "",
    description: "",
    userId,
  });
  const [createSuccess, setCreateSuccess] = useState<boolean | null>(null);

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    console.log({ formData });
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const response = await fetch("/api/categories", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    if (!response.ok) {
      setCreateSuccess(false);
    } else {
      setCreateSuccess(true);
      refetchCategories();
      setTimeout(() => {
        closeNewCategoryModal();
        setCreateSuccess(null);
      }, 3000);
    }
  };

  return (
    <Modal open={open} onClose={closeNewCategoryModal}>
      <Box sx={{ ...modalStyle, width: modalWidth }}>
        <Grid container spacing={{ xs: 2, md: 3 }}>
          <Grid item xs={12}>
            <Typography variant="h5">Add New Category</Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormControl fullWidth>
              <InputLabel>Name</InputLabel>
              <Input
                id="name"
                name="name"
                type="text"
                value={formData.name}
                onChange={handleChange}
                required
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormControl fullWidth>
              <InputLabel>Description</InputLabel>
              <Input
                id="description"
                name="description"
                type="text"
                value={formData.description}
                onChange={handleChange}
                required
              />
            </FormControl>
          </Grid>

          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Button variant="contained" onClick={handleSubmit}>
              Create
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={closeNewCategoryModal}
            >
              Cancel
            </Button>
          </Grid>
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {isBoolean(createSuccess) && !createSuccess && (
              <CustomAlert
                type="error"
                content="Unable to create category. Please try again"
              />
            )}
            {isBoolean(createSuccess) && createSuccess && (
              <CustomAlert
                type="success"
                content="Category successfully added"
              />
            )}
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
};

export default AddCategoryModal;
