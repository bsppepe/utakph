import React, { memo, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Paper, { PaperProps } from "@mui/material/Paper";
import Draggable from "react-draggable";
import CustomAlert from "./Alert";
import { isBoolean } from "lodash";

interface IDeleteProductDialog {
  open: boolean;
  handleClose: () => void;
  name: string;
  refetchProducts: () => void;
  productId: string;
}

function PaperComponent(props: PaperProps) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const DeleteProductDialog = memo<IDeleteProductDialog>(
  ({
    open,
    handleClose,
    name,
    refetchProducts,
    productId,
  }: IDeleteProductDialog) => {
    const [deleteSuccess, setDeleteSuccess] = useState<boolean | null>();

    const handleDelete = async () => {
      const response = await fetch(`/api/products?id=${productId}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (!response.ok) {
        setDeleteSuccess(false);
      } else {
        setDeleteSuccess(true);
        refetchProducts();
        setTimeout(() => {
          handleClose();
        }, 2000);
      }
    };

    return (
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Delete {name}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete product? It cannot be recovered once
            removed
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Cancel
          </Button>
          <Button onClick={handleDelete}>Delete</Button>
        </DialogActions>
        {isBoolean(deleteSuccess) && !deleteSuccess && (
          <CustomAlert type="error" content="Unable to delete product." />
        )}
        {isBoolean(deleteSuccess) && deleteSuccess && (
          <CustomAlert type="success" content="Product successfully deleted" />
        )}
      </Dialog>
    );
  }
);

DeleteProductDialog.displayName = "Delete Product";

export default DeleteProductDialog;
