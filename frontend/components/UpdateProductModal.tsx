import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  Input,
  InputLabel,
  ListItemText,
  MenuItem,
  Modal,
  OutlinedInput,
  Radio,
  RadioGroup,
  Select,
  Typography,
} from "@mui/material";
import { modalStyle } from "../pages/UserSelectionPage/styles";
import { SIZE_SELECTION } from "@/utils/constants";
import CustomAlert from "./Alert";
import { isBoolean } from "lodash";

interface INewProductModal {
  open: boolean;
  closeUpdateProductModal: () => void;
  refetchProducts: () => void;
  userId: string;
  name: string;
  amountInStock: number;
  cost: number;
  category: string;
  sizes: string[];
  isAvailable: boolean;
  id: string;
  categories: any;
}

interface IUpdateProduct {
  userId: string;
  name: string;
  amountInStock: number;
  cost: number;
  category: string;
  sizes: string[];
  isAvailable: boolean;
  id: string;
}

const UpdateProductModal = ({
  open,
  closeUpdateProductModal,
  refetchProducts,
  userId,
  name,
  amountInStock,
  cost,
  category,
  sizes,
  isAvailable,
  id,
  categories,
}: INewProductModal) => {
  const [successUpdateProduct, setSuccessUpdateProduct] = useState<
    boolean | null
  >(null);

  const [modalWidth, setModalWidth] = useState("50%");

  useEffect(() => {
    if (typeof window !== "undefined") {
      const isSmallScreen = window.matchMedia("(max-width: 600px)").matches;
      setModalWidth(isSmallScreen ? "90%" : "50%");
    }
  }, []);

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const defaultValue = {
    userId,
    name,
    amountInStock,
    cost,
    category,
    sizes,
    isAvailable,
    id,
  };

  const [formData, setFormData] = useState(defaultValue);

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const response = await fetch("/api/products", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    if (!response.ok) {
      setSuccessUpdateProduct(false);
    } else {
      setSuccessUpdateProduct(true);
      refetchProducts();
      setFormData(formData);
      setTimeout(() => {
        closeUpdateProductModal();
        setSuccessUpdateProduct(null);
      }, 2000);
    }
  };

  return (
    <Modal open={open} onClose={closeUpdateProductModal}>
      <Box sx={{ ...modalStyle, width: modalWidth }}>
        <Grid container spacing={{ xs: 2, md: 3 }}>
          <Grid item xs={12}>
            <Typography variant="h5">Update Product</Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <InputLabel>Name</InputLabel>
              <Input
                id="name"
                name="name"
                type="text"
                value={formData.name}
                onChange={handleChange}
                required
              />
            </FormControl>
          </Grid>

          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <InputLabel>Stocks</InputLabel>
              <Input
                id="amountInStock"
                name="amountInStock"
                type="number"
                value={formData.amountInStock}
                onChange={handleChange}
                required
              />
              <FormHelperText>Number of stocks available</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <InputLabel>Cost</InputLabel>
              <Input
                id="cost"
                name="cost"
                type="number"
                value={formData.cost}
                onChange={handleChange}
                required
              />
              <FormHelperText>Price per item</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <FormLabel>Category</FormLabel>
              <Select
                id="category"
                name="category"
                type="text"
                value={formData.category}
                onChange={handleChange}
                size="small"
                required
              >
                {" "}
                {categories &&
                  categories.length > 0 &&
                  categories.map((category: any, idx: any) => {
                    return (
                      <MenuItem value={category.id} key={idx}>
                        {category.name}
                      </MenuItem>
                    );
                  })}
              </Select>
              <FormHelperText>
                Not available yet? You can create new categories
              </FormHelperText>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <FormLabel>Size/s</FormLabel>
              <Select
                id="sizes"
                name="sizes"
                type="text"
                value={formData.sizes}
                onChange={handleChange}
                input={<OutlinedInput label="Tag" />}
                MenuProps={MenuProps}
                renderValue={(selected) => selected.join(", ")}
                size="small"
                multiple
                required
              >
                {SIZE_SELECTION.map((size: string, idx: any) => {
                  return (
                    <MenuItem value={`${size}`} key={idx}>
                      <Checkbox checked={formData.sizes.indexOf(size) > -1} />
                      <ListItemText primary={size} />
                    </MenuItem>
                  );
                })}
              </Select>
              <FormHelperText style={{ width: "100%" }}>
                You can choose multiple sizes per item
              </FormHelperText>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={4}>
            <FormControl component="fieldset" fullWidth margin="dense">
              <FormLabel>Available</FormLabel>

              <RadioGroup
                id="isAvailable"
                name="isAvailable"
                defaultValue={formData.isAvailable ? "Yes" : "No"}
                value={formData.isAvailable}
                onChange={handleChange}
                row
              >
                <FormControlLabel
                  value={true}
                  control={<Radio />}
                  label="Yes"
                />
                <FormControlLabel
                  value={false}
                  control={<Radio />}
                  label="No"
                />
              </RadioGroup>
              <FormHelperText>Visible to clients</FormHelperText>
            </FormControl>
          </Grid>
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Button variant="contained" onClick={handleSubmit}>
              Update
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={closeUpdateProductModal}
            >
              Cancel
            </Button>
          </Grid>
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {isBoolean(successUpdateProduct) && !successUpdateProduct && (
              <CustomAlert
                type="error"
                content="Unable to update product. Please try again"
              />
            )}
            {isBoolean(successUpdateProduct) && successUpdateProduct && (
              <CustomAlert
                type="success"
                content="Product successfully updated"
              />
            )}
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
};

export default UpdateProductModal;
