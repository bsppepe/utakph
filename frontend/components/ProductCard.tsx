import React, { useCallback, useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Chip } from "@mui/material";
import { lightBlue } from "@mui/material/colors";
import DeleteProductDialog from "./DeleteProductDialog";
import UpdateProductModal from "./UpdateProductModal";

interface IProductCard {
  name: string;
  category: any;
  amountInStock: number;
  cost: number;
  isAvailable: boolean;
  size: any;
  productId: string;
  key: any;
  userId: string;
  categories: any;
  refetchProducts: () => void;
}

const ProductCard: React.FC<IProductCard> = ({
  name,
  category,
  amountInStock,
  cost,
  isAvailable,
  size,
  productId,
  userId,
  key,
  categories,
  refetchProducts,
}) => {
  const [categoryName, setCategoryName] = useState("");
  const [sizes, setSizes] = useState([]);

  const processData = useCallback(async () => {
    const foundCategory = categories.find((item: any) => item.id === category);
    if (foundCategory) setCategoryName(foundCategory.name);
    setSizes(Object.values(size));
  }, [categories, category, size]);

  useEffect(() => {
    processData();
  }, [processData]);

  const [openDelete, setOpenDelete] = useState(false);
  const handleOpenDelete = () => setOpenDelete(true);
  const handleCloseDelete = () => setOpenDelete(false);

  const [openUpdateProduct, setOpenUpdateProduct] = useState<boolean>(false);
  const handleCloseUpdateProduct = () => setOpenUpdateProduct(false);
  const handleOpenUpdateProduct = () => setOpenUpdateProduct(true);

  return (
    <Card
      sx={{ margin: 2, backgroundColor: `${lightBlue[400]} 0.1` }}
      variant="outlined"
    >
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {`Category: ${categoryName}`}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Sizes:{" "}
          {sizes &&
            sizes.length > 0 &&
            sizes.map((item: any, idx: any) => (
              <Chip
                label={item}
                size="small"
                style={{ marginLeft: 5 }}
                key={idx}
              />
            ))}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {`Stock Available: ${amountInStock}`}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {`Cost: ${cost}`}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Status:{" "}
          <Chip
            label={isAvailable ? "Available" : "Archived"}
            size="small"
            color={isAvailable ? "success" : "error"}
            style={{ marginLeft: 5 }}
            variant="outlined"
          />
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={handleOpenUpdateProduct}>
          edit
        </Button>
        <Button size="small" onClick={handleOpenDelete}>
          delete
        </Button>
      </CardActions>
      <DeleteProductDialog
        name={name}
        productId={productId}
        open={openDelete}
        handleClose={handleCloseDelete}
        refetchProducts={refetchProducts}
      />
      <UpdateProductModal
        amountInStock={amountInStock}
        categories={categories}
        category={category}
        closeUpdateProductModal={handleCloseUpdateProduct}
        cost={cost}
        id={productId}
        isAvailable={isAvailable}
        name={name}
        open={openUpdateProduct}
        refetchProducts={refetchProducts}
        sizes={sizes}
        userId={userId}
      />
    </Card>
  );
};

export default ProductCard;
