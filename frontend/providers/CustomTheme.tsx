import { ThemeProvider, createTheme } from "@mui/material";
import { lightBlue } from "@mui/material/colors";
import React, { ReactNode } from "react";

const customTheme = createTheme({
  palette: {
    primary: {
      main: lightBlue[400],
    },
  },
  typography: {
    fontFamily: "Poppins, sans-serif",
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
    h1: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 700,
      fontSize: "2.5rem",
    },
    h2: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 700,
      fontSize: "2rem",
    },
    h3: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 700,
      fontSize: "1.75rem",
    },
    h4: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 700,
      fontSize: "1.5rem",
    },
    h5: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 700,
      fontSize: "1.25rem",
    },
    h6: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 700,
      fontSize: "1.125rem",
    },
    body1: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 400,
      fontSize: "1rem",
    },
    body2: {
      fontFamily: "Poppins, sans-serif",
      fontWeight: 400,
      fontSize: "0.875rem",
    },
  },

  components: {
    MuiCssBaseline: {
      styleOverrides: {
        "@font-face": [
          {
            fontFamily: "Poppins",
            fontStyle: "normal",
            fontWeight: 400,
          },
        ],
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          borderColor: lightBlue[500],
        },
      },
    },
    MuiModal: {
      styleOverrides: {
        root: {
          border: "2px solid your-desired-color",
        },
      },
    },
  },
});

const MuiThemeProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  return <ThemeProvider theme={customTheme}>{children}</ThemeProvider>;
};

export default MuiThemeProvider;
