import { createContext, useReducer, ReactNode, useEffect } from "react";

const initialStateUser = {
  name: null,
  emailAddress: null,
  userId: null,
};

interface IUserData {
  name: string | null;
  emailAddress: string | null;
  userId: string | null;
}

interface AddUserAction {
  type: "ADD_USER";
  userId: string | null;
  name: string | null;
  emailAddress: string | null;
}

interface RemoveUserAction {
  type: "REMOVE_USER";
}

type UserAction = AddUserAction | RemoveUserAction;

const reducer = (state: IUserData, action: UserAction) => {
  switch (action.type) {
    case "ADD_USER":
      state = {
        userId: action.userId,
        name: action.name,
        emailAddress: action.emailAddress,
      };
      return state;
    case "REMOVE_USER":
      return initialStateUser;
  }
};

interface UserContextProps {
  userData: IUserData;
  updateUser: React.Dispatch<AddUserAction>;
}

export const UserContext = createContext<UserContextProps | undefined>(
  undefined
);

const UserProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [userData, updateUser] = useReducer(reducer, initialStateUser);

  return (
    <UserContext.Provider value={{ userData, updateUser }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
