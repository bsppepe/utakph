import React, { useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { GoogleFonts } from "next-google-fonts";
import createCache from "@emotion/cache";
import UserProvider from "./providers/UserProvider";
import MuiThemeProvider from "./providers/CustomTheme";

import { CacheProvider } from "@emotion/react";

import MainPage from "./pages/MainPage";

const cache = createCache({ key: "css", prepend: true });

const App: React.FC = () => {
  useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      //@ts-ignore
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);
  return (
    <Router>
      <UserProvider>
        <CacheProvider value={cache}>
          <GoogleFonts href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700&display=swap" />
          <MuiThemeProvider>
            <MainPage />
          </MuiThemeProvider>
        </CacheProvider>
      </UserProvider>
    </Router>
  );
};

export default App;
