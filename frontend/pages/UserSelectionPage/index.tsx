import React, { memo, useCallback, useEffect, useState } from "react";
import { CustomButton, CustomContainer } from "./styles";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import SelectUserModal from "@/frontend/components/SelectUserModal";
import AddUserModal from "@/frontend/components/AddUserModal";

const UserSelectionPage = memo(() => {
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);

  const [openSelectUserModal, setOpenSelectUserModal] = useState(false);
  const [openCreateUserModal, setOpenCreateUserModal] = useState(false);

  const handleOpenSelectUser = () => setOpenSelectUserModal(true);
  const handleCloseSelectUser = () => {
    setName(null);
    setOpenSelectUserModal(false);
  };
  const handleOpenAddUser = () => setOpenCreateUserModal(true);
  const handleCloseAddUser = () => {
    setEmail(null);
    setOpenCreateUserModal(false);
  };

  const [users, setUsers] = useState<any>(null);

  const fetchUsers = useCallback(async () => {
    try {
      const rawResponse = await fetch("/api/users", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (!rawResponse.ok) {
        throw new Error("Failed to fetch users");
      }

      const response = await rawResponse.json();
      setUsers(response.data);
    } catch (error) {
      console.error("Error fetching users:", error);
      setUsers(null);
    }
  }, []);

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  return (
    <CustomContainer>
      <CustomButton
        variant="contained"
        onClick={handleOpenSelectUser}
        startIcon={<PersonOutlineIcon />}
      >
        Select User
      </CustomButton>
      <CustomButton
        variant="contained"
        onClick={handleOpenAddUser}
        startIcon={<PersonAddAltIcon />}
      >
        Add User
      </CustomButton>

      <SelectUserModal
        handleCloseSelectUser={handleCloseSelectUser}
        openSelectUserModal={openSelectUserModal}
        users={users}
      />
      <AddUserModal
        name={name}
        setName={setName}
        email={email}
        setEmail={setEmail}
        handleCloseAddUser={handleCloseAddUser}
        openCreateUserModal={openCreateUserModal}
        fetchUsers={fetchUsers}
      />
    </CustomContainer>
  );
});

UserSelectionPage.displayName = "Select User Page";

export default UserSelectionPage;
