import { styled } from "@mui/material";
import Button, { ButtonProps } from "@mui/material/Button";
import { lightBlue } from "@mui/material/colors";

export const CustomContainer = styled("div")({
  height: "100vh",
  width: "100vw",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
});

export const CustomButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: "white",
  backgroundColor: lightBlue[500],
  "&:hover": {
    backgroundColor: lightBlue[700],
  },
  borderWidth: "3px",
  height: "10rem",
  width: "10rem",
  margin: "2rem",
}));


export const modalStyle = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "40%",
  bgcolor: "background.paper",
  border: "2px solid #ADD8E6",
  p: 4,
  borderRadius: 5,
};
