import ButtonAppBar from "@/frontend/components/AppBar";
import ProductsContainer from "@/frontend/components/ProductContainer";
import React from "react";

const ProductsPage = () => {
  return (
    <React.Fragment>
      <ButtonAppBar />
      <ProductsContainer />
    </React.Fragment>
  );
};

export default ProductsPage;
