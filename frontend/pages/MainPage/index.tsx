import { UserContext } from "@/frontend/providers/UserProvider";
import React, { useContext, useEffect } from "react";
import UserSelectionPage from "../UserSelectionPage";
import ProductsPage from "../ProductsPage";

const MainPage = () => {
  //@ts-ignore
  const { userData } = useContext(UserContext);

  useEffect(() => {}, [userData]);

  return (
    <React.Fragment>
      {!userData.userId && <UserSelectionPage />}
      {userData.userId && <ProductsPage />}
    </React.Fragment>
  );
};

export default MainPage;
