import { ref, set, remove, onValue } from "firebase/database";
import { database } from "./firebase";
import { DATABASE_ENTRIES, FUNCTIONS } from "@/utils/constants";

export interface IFirebaseDB {
  entry: DATABASE_ENTRIES;
  func: FUNCTIONS;
  id?: any;
  params?: any;
  exist?: { key: string; value: any };
}

const firebaseDB = async (args: IFirebaseDB): Promise<any> => {
  try {
    console.log({
      id: "firebaseDB-process-start",
      args,
    });

    let response;

    switch (args.func) {
      case FUNCTIONS.CREATE:
      case FUNCTIONS.UPDATE:
        response = await set(
          ref(database, `${args.entry}/${args.id}`),
          args.params
        );
        break;
      case FUNCTIONS.READ:
        await new Promise<void>((resolve) => {
          onValue(ref(database, `${args.entry}/${args.id}`), (snapshot) => {
            response = snapshot.toJSON();
            resolve();
          });
        });
        break;
      case FUNCTIONS.LIST:
        await new Promise<void>((resolve) => {
          onValue(ref(database, `${args.entry}/`), (snapshot) => {
            const arrayList: any[] = [];

            snapshot.forEach((child) => {
              arrayList.push({
                id: child.key,
                ...child.toJSON(),
              });
            });

            response = arrayList;
            resolve();
          });
        });
        break;
      case FUNCTIONS.EXIST:
        await new Promise<void>((resolve) => {
          onValue(ref(database, `${args.entry}/`), (snapshot) => {
            snapshot.forEach((child) => {
              const childData: any = child.toJSON();
              if (childData[args.exist!.key] === args.exist!.value)
                response = true;
            });

            resolve();
          });
        });
        break;
      case FUNCTIONS.DELETE:
        response = await remove(ref(database, `${args.entry}/${args.id}`));
        break;
      default:
        console.error({
          id: `firebase-database-${args.func}-error`,
        });
        throw new Error(
          "Unable to process database, please select a valid function"
        );
    }

    console.log({
      id: "firebaseDB-process-end",
      args,
      response,
    });

    return response;
  } catch (error: any) {
    console.error({
      id: "firebaseDB-error",
      args,
      error,
    });
    throw new Error("Unable to complete database function");
  }
};

export default firebaseDB;
