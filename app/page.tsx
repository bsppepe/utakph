"use client";

import React from "react";
import "@fontsource/poppins";
import App from "@/frontend/router";

export default function Home() {
  return (
    <React.Fragment>{typeof window !== "undefined" && <App />}</React.Fragment>
  );
}
