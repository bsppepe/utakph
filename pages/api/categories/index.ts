import { NextApiRequest, NextApiResponse } from "next";
import nextConnect from "next-connect";
import getHandler from "../../../backend/handlers/categories/get";
import newHandler from "../../../backend/handlers/categories/new";

export default nextConnect<NextApiRequest, NextApiResponse>({
  onNoMatch(req: any, res: any) {
    res.status(400).json({
      errorMessage: "400 Bad Request",
    });
  },
})
  .post(newHandler)
  .get(getHandler);
