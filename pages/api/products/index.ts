import { NextApiRequest, NextApiResponse } from "next";
import nextConnect from "next-connect";
import updateHandler from "../../../backend/handlers/product/update";
import newHandler from "../../../backend/handlers/product/new";
import getHandler from "../../../backend/handlers/product/get";
import deleteHandler from "../../../backend/handlers/product/delete";

export default nextConnect<NextApiRequest, NextApiResponse>({
  onNoMatch(req: any, res: any) {
    res.status(400).json({
      errorMessage: "400 Bad Request",
    });
  },
})
  .post(newHandler)
  .put(updateHandler)
  .get(getHandler)
  .delete(deleteHandler);
